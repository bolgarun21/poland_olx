#!/usr/bin/sh
import re
import time
import json
import requests
import psycopg2
import logging

from datetime import datetime
from bs4 import BeautifulSoup
from telegram import send_message


url = 'https://www.olx.pl/d/nieruchomosci/mieszkania/wynajem/zielonagora/?search%5Bprivate_business%5D=private&search%5Bfilter_float_price:to%5D=2300&search%5Bfilter_enum_rooms%5D%5B0%5D=two&search%5Bfilter_enum_rooms%5D%5B1%5D=three'

headers = {
    'USER_AGENT':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'
    }


class OLXParser():
    def __init__(self):
        self.conn = None
        self.cursor = None
        self.advertisement_urls = []
        self.new_urls = []
        self.data = []

    def connect_to_database(self):
        # connect to the PostgreSQL server
        self.conn = psycopg2.connect(
            database="olx",
            user="postgres",
            password="postgres",
            host="172.17.0.2",
            port='5432'
            )

        # Creating a cursor object
        self.cursor = self.conn.cursor()

    def filter_database(self):
        self.cursor.execute("SELECT url FROM advertisement_pl")

        result = self.cursor.fetchall()
        self.advertisement_urls += [x[0] for x in result]

    def parser(self):
        self.connect_to_database()
        self.filter_database()

        response = requests.get(url, headers=headers)
        # time.sleep(20)

        if response.status_code == 200:
            html = response.content
            soup = BeautifulSoup(html, 'lxml')
            location = soup.find_all('div', {'class': 'css-19ucd76'})

            for x in location:
                try:
                    link = x.a['href']
                except Exception:
                    pass

                if 'http' not in link:
                    link = 'https://www.olx.pl' + link

                if (
                    link not in self.advertisement_urls and
                    link not in self.new_urls
                ):
                    self.data.append(
                        (
                            link,
                            datetime.now()
                            )
                        )

                    send_message(text=link)
                    self.new_urls.append(link)

        print('End parser - [{}]'.format(datetime.now()))

    def write_to_database(self):
        sql = "INSERT INTO advertisement_pl (url, created_at) VALUES (%s,%s)"

        # executing the sql statement
        self.cursor.executemany(sql,self.data)

        # commiting changes
        self.conn.commit()
         
        # closing connection
        self.conn.close()


logging.basicConfig(
    filename="myscript.log", 
    format='%(asctime)s %(message)s', 
    filemode='w'
    )

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.warning("OOPS!!!Its a Warning")


olx = OLXParser()
olx.parser()
olx.write_to_database()