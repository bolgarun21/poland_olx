import sys
import psycopg2


def create_database():
    # connect to the PostgreSQL server
    conn = psycopg2.connect(
        database="postgres",
        user="postgres",
        password="postgres",
        host="172.28.1.4",
        port=5432
        )

    # Creating a cursor object
    cursor = conn.cursor()

    conn.autocommit = True

    # query to create a database
    sql = ''' CREATE database olx ENCODING 'UTF-8' LC_COLLATE 'uk_UA.UTF-8' LC_CTYPE 'uk_UA.UTF-8' TEMPLATE template0;'''

    # executing above query
    try:
        cursor.execute(sql)
        print("Database has been created successfully !!")
    except Exception as ex:
        print('Failed to create the database because:', ex)
    finally:
        conn.close()

def create_tables():
    # connect to the PostgreSQL server
    conn = psycopg2.connect(
        database="olx",
        user="postgres",
        password="postgres",
        host="172.28.1.4",
        port=5432
        )

    # Creating a cursor object
    cursor = conn.cursor()

    """ create tables in the PostgreSQL database"""
    command = (
        """ CREATE TABLE advertisement_pl (
                id SERIAL PRIMARY KEY,
                url text,
                created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
                )
        """
        )

    try:
        cursor.execute(command)

        # close communication with the PostgreSQL database server
        cursor.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        conn.close()


if __name__ == '__main__':
    create_database()
    create_tables()